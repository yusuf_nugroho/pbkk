package com.pendidikan.dataPokokDomain;

import java.io.Serializable;
import java.io.Serializable;
import java.util.Date;

import org.joda.time.DateTime;

public class Wali_pd implements Serializable{

	private String ID_WALI;
	private String ID_PD;
	private String NO_AKTE;
	private String NO_KK;
	private String NAMA_AYAH;
	private String NAMA_IBU;
	private Date CREATE_DATE;
	private int SOFT_DELETE;
	
	public String getID_WALI() {
		return ID_WALI;
	}
	public void setID_WALI(String iD_WALI) {
		ID_WALI = iD_WALI;
	}
	public String getID_PD() {
		return ID_PD;
	}
	public void setID_PD(String iD_PD) {
		ID_PD = iD_PD;
	}
	public String getNO_AKTE() {
		return NO_AKTE;
	}
	public void setNO_AKTE(String nO_AKTE) {
		NO_AKTE = nO_AKTE;
	}
	public String getNO_KK() {
		return NO_KK;
	}
	public void setNO_KK(String nO_KK) {
		NO_KK = nO_KK;
	}
	public String getNAMA_AYAH() {
		return NAMA_AYAH;
	}
	public void setNAMA_AYAH(String nAMA_AYAH) {
		NAMA_AYAH = nAMA_AYAH;
	}
	public String getNAMA_IBU() {
		return NAMA_IBU;
	}
	public void setNAMA_IBU(String nAMA_IBU) {
		NAMA_IBU = nAMA_IBU;
	}
	public Date getCREATE_DATE() {
		return CREATE_DATE;
	}
	public void setCREATE_DATE(Date cREATE_DATE) {
		CREATE_DATE = cREATE_DATE;
	}
	public int getSOFT_DELETE() {
		return SOFT_DELETE;
	}
	public void setSOFT_DELETE(int sOFT_DELETE) {
		SOFT_DELETE = sOFT_DELETE;
	}
}
