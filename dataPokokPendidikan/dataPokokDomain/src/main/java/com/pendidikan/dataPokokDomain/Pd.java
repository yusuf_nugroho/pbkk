package com.pendidikan.dataPokokDomain;
import java.io.Serializable;
import java.util.Date;

import org.joda.time.DateTime;
public class Pd implements Serializable {
	private String ID_PD;
	private String ID_WALI;
	private String NIM_PD;
	private String NM_PD;
	private String AL_PD;
	private String TMPT_PD;
	private Date TGLLHR_PD;
	private String JK_PD;
	private String GOLONGAN_DARAH_PESERTA_DIDIK;
	private String AGAMA_PD;
	private Date CREATE_DATE;
	private int SOFT_DELETE;
	
	public String getID_PD() {
		return ID_PD;
	}
	public void setID_PD(String iD_PD) {
		ID_PD = iD_PD;
	}
	public String getID_WALI() {
		return ID_WALI;
	}
	public void setID_WALI(String iD_WALI) {
		ID_WALI = iD_WALI;
	}
	public String getNIM_PD() {
		return NIM_PD;
	}
	public void setNIM_PD(String nIM_PD) {
		NIM_PD = nIM_PD;
	}
	public String getNM_PD() {
		return NM_PD;
	}
	public void setNM_PD(String nM_PD) {
		NM_PD = nM_PD;
	}
	public String getAL_PD() {
		return AL_PD;
	}
	public void setAL_PD(String aL_PD) {
		AL_PD = aL_PD;
	}
	public String getTMPT_PD() {
		return TMPT_PD;
	}
	public void setTMPT_PD(String tMPT_PD) {
		TMPT_PD = tMPT_PD;
	}
	public Date getTGLLHR_PD() {
		return TGLLHR_PD;
	}
	public void setTGLLHR_PD(Date tGLLHR_PD) {
		TGLLHR_PD = tGLLHR_PD;
	}
	public String getJK_PD() {
		return JK_PD;
	}
	public void setJK_PD(String jK_PD) {
		JK_PD = jK_PD;
	}
	
	public String getAGAMA_PD() {
		return AGAMA_PD;
	}
	public void setAGAMA_PD(String aGAMA_PD) {
		AGAMA_PD = aGAMA_PD;
	}
	public Date getCREATE_DATE() {
		return CREATE_DATE;
	}
	public void setCREATE_DATE(Date cREATE_DATE) {
		CREATE_DATE = cREATE_DATE;
	}
	public int getSOFT_DELETE() {
		return SOFT_DELETE;
	}
	public void setSOFT_DELETE(int sOFT_DELETE) {
		SOFT_DELETE = sOFT_DELETE;
	}
	public String getGOLONGAN_DARAH_PESERTA_DIDIK() {
		return GOLONGAN_DARAH_PESERTA_DIDIK;
	}
	public void setGOLONGAN_DARAH_PESERTA_DIDIK(
			String gOLONGAN_DARAH_PESERTA_DIDIK) {
		GOLONGAN_DARAH_PESERTA_DIDIK = gOLONGAN_DARAH_PESERTA_DIDIK;
	}
}

