package com.pendidikan.dataPokokDomain;

import java.io.Serializable;
import java.util.Date;

import org.joda.time.DateTime;

public class Ptk implements Serializable {
	private int ID_PTK;
	private String NIP_PTK;
	private String NM_PTK;
	private Date CREATE_DATE;
	private int SOFT_DELETE;

	public int getID_PTK() {
		return ID_PTK;
	}

	public void setID_PTK(int iD_PTK) {
		ID_PTK = iD_PTK;
	}

	public String getNIP_PTK() {
		return NIP_PTK;
	}

	public void setNIP_PTK(String nIP_PTK) {
		NIP_PTK = nIP_PTK;
	}

	public String getNM_PTK() {
		return NM_PTK;
	}

	public void setNM_PTK(String nM_PTK) {
		NM_PTK = nM_PTK;
	}

	public Date getCREATE_DATE() {
		return CREATE_DATE;
	}

	public void setCREATE_DATE(Date cREATE_DATE) {
		CREATE_DATE = cREATE_DATE;
	}

	public int getSOFT_DELETE() {
		return SOFT_DELETE;
	}

	public void setSOFT_DELETE(int sOFT_DELETE) {
		SOFT_DELETE = sOFT_DELETE;
	}

}
