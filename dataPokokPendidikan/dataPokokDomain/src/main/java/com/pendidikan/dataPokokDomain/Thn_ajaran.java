package com.pendidikan.dataPokokDomain;

import java.io.Serializable;
import java.util.Date;

import org.joda.time.DateTime;

public class Thn_ajaran implements Serializable{
	private String ID_THN_AJARAN;
	private int THN_THN_AJARAN;
	private Date CREATE_DATE;
	private int SOFT_DELETE;
	
	public String getID_THN_AJARAN() {
		return ID_THN_AJARAN;
	}
	public void setID_THN_AJARAN(String iD_THN_AJARAN) {
		ID_THN_AJARAN = iD_THN_AJARAN;
	}
	public int getTHN_THN_AJARAN() {
		return THN_THN_AJARAN;
	}
	public void setTHN_THN_AJARAN(int tHN_THN_AJARAN) {
		THN_THN_AJARAN = tHN_THN_AJARAN;
	}
	public Date getCREATE_DATE() {
		return CREATE_DATE;
	}
	public void setCREATE_DATE(Date cREATE_DATE) {
		CREATE_DATE = cREATE_DATE;
	}
	public int getSOFT_DELETE() {
		return SOFT_DELETE;
	}
	public void setSOFT_DELETE(int sOFT_DELETE) {
		SOFT_DELETE = sOFT_DELETE;
	}

}
