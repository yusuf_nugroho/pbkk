package com.pendidikan.dataPokokDomain;

import java.io.Serializable;
import java.io.Serializable;
import java.util.Date;

import org.joda.time.DateTime;

public class Rombel implements Serializable{
	private String ID_ROMBEL;
	private int ID_PTK;
	private String ID_MATPEL;
	private String ID_SMT;
	private String NM_ROMBBEL;
	private int JML_ROMBBEL;
	private int PERTEMUAN_DALAM_SEMINGGU;
	private Date CREATE_DATE;
	private int SOFT_DELETE;
	
	public String getID_ROMBEL() {
		return ID_ROMBEL;
	}
	public void setID_ROMBEL(String iD_ROMBEL) {
		ID_ROMBEL = iD_ROMBEL;
	}
	public int getID_PTK() {
		return ID_PTK;
	}
	public void setID_PTK(int iD_PTK) {
		ID_PTK = iD_PTK;
	}
	public String getID_MATPEL() {
		return ID_MATPEL;
	}
	public void setID_MATPEL(String iD_MATPEL) {
		ID_MATPEL = iD_MATPEL;
	}
	public String getID_SMT() {
		return ID_SMT;
	}
	public void setID_SMT(String iD_SMT) {
		ID_SMT = iD_SMT;
	}
	public String getNM_ROMBBEL() {
		return NM_ROMBBEL;
	}
	public void setNM_ROMBBEL(String nM_ROMBBEL) {
		NM_ROMBBEL = nM_ROMBBEL;
	}
	public int getJML_ROMBBEL() {
		return JML_ROMBBEL;
	}
	public void setJML_ROMBBEL(int jML_ROMBBEL) {
		JML_ROMBBEL = jML_ROMBBEL;
	}
	public int getPERTEMUAN_DALAM_SEMINGGU() {
		return PERTEMUAN_DALAM_SEMINGGU;
	}
	public void setPERTEMUAN_DALAM_SEMINGGU(int pERTEMUAN_DALAM_SEMINGGU) {
		PERTEMUAN_DALAM_SEMINGGU = pERTEMUAN_DALAM_SEMINGGU;
	}
	public Date getCREATE_DATE() {
		return CREATE_DATE;
	}
	public void setCREATE_DATE(Date cREATE_DATE) {
		CREATE_DATE = cREATE_DATE;
	}
	public int getSOFT_DELETE() {
		return SOFT_DELETE;
	}
	public void setSOFT_DELETE(int sOFT_DELETE) {
		SOFT_DELETE = sOFT_DELETE;
	}
}
