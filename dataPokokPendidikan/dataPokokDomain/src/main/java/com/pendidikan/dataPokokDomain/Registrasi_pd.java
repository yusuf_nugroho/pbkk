package com.pendidikan.dataPokokDomain;

import java.io.Serializable;

import org.joda.time.DateTime;

import java.io.Serializable;
import java.util.Date;

public class Registrasi_pd implements Serializable {
	private String ID_REGISTRASI;
	private String ID_SAT_MAN;
	private String ID_PD;
	private Date TANGGAL_MASUK;
	private Date TANGGAL_KELUAR;
	private Date CREATE_DATE;
	private int SOFT_DELETE;
	
	public String getID_REGISTRASI() {
		return ID_REGISTRASI;
	}
	public void setID_REGISTRASI(String iD_REGISTRASI) {
		ID_REGISTRASI = iD_REGISTRASI;
	}
	public String getID_SAT_MAN() {
		return ID_SAT_MAN;
	}
	public void setID_SAT_MAN(String iD_SAT_MAN) {
		ID_SAT_MAN = iD_SAT_MAN;
	}
	public String getID_PD() {
		return ID_PD;
	}
	public void setID_PD(String iD_PD) {
		ID_PD = iD_PD;
	}
	public Date getTANGGAL_MASUK() {
		return TANGGAL_MASUK;
	}
	public void setTANGGAL_MASUK(Date tANGGAL_MASUK) {
		TANGGAL_MASUK = tANGGAL_MASUK;
	}
	public Date getTANGGAL_KELUAR() {
		return TANGGAL_KELUAR;
	}
	public void setTANGGAL_KELUAR(Date tANGGAL_KELUAR) {
		TANGGAL_KELUAR = tANGGAL_KELUAR;
	}
	public int getSOFT_DELETE() {
		return SOFT_DELETE;
	}
	public void setSOFT_DELETE(int sOFT_DELETE) {
		SOFT_DELETE = sOFT_DELETE;
	}
	public Date getCREATE_DATE() {
		return CREATE_DATE;
	}
	public void setCREATE_DATE(Date cREATE_DATE) {
		CREATE_DATE = cREATE_DATE;
	}
}
