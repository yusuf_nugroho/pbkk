package com.pendidikan.dataPokokDomain;
import java.io.Serializable;
import java.util.Date;

import org.joda.time.DateTime;

public class nilai_akhir implements Serializable {
	private String ID_NILAIAKHIR;
	private int NILAI_UJIANAKHIR;
	private int NILAI_UAS;
	private Date CREATE_DATE;
	private int SOFT_DELETE;
	
	public String getID_NILAIAKHIR() {
		return ID_NILAIAKHIR;
	}
	public void setID_NILAIAKHIR(String iD_NILAIAKHIR) {
		ID_NILAIAKHIR = iD_NILAIAKHIR;
	}
	public int getNILAI_UJIANAKHIR() {
		return NILAI_UJIANAKHIR;
	}
	public void setNILAI_UJIANAKHIR(int nILAI_UJIANAKHIR) {
		NILAI_UJIANAKHIR = nILAI_UJIANAKHIR;
	}
	public int getNILAI_UAS() {
		return NILAI_UAS;
	}
	public void setNILAI_UAS(int nILAI_UAS) {
		NILAI_UAS = nILAI_UAS;
	}
	public Date getCREATE_DATE() {
		return CREATE_DATE;
	}
	public void setCREATE_DATE(Date cREATE_DATE) {
		CREATE_DATE = cREATE_DATE;
	}
	public int getSOFT_DELETE() {
		return SOFT_DELETE;
	}
	public void setSOFT_DELETE(int sOFT_DELETE) {
		SOFT_DELETE = sOFT_DELETE;
	}
}
