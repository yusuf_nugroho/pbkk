package com.pendidikan.dataPokokDomain;

import java.io.Serializable;
import java.util.Date;

import org.joda.time.DateTime;

public class anggota_rombel {
	private String ID_ANGGOTA_ROMBEL;
	private String ID_PD;
	private String ID_ROMBEL;
	private Date CREATE_DATE;
	private int SOFT_DELETE;
	
	public String getID_PD() {
		return ID_PD;
	}
	public void setID_PD(String iD_PD) {
		ID_PD = iD_PD;
	}
	public String getID_ROMBEL() {
		return ID_ROMBEL;
	}
	public void setID_ROMBEL(String iD_ROMBEL) {
		ID_ROMBEL = iD_ROMBEL;
	}
	public Date getCREATE_DATE() {
		return CREATE_DATE;
	}
	public void setCREATE_DATE(Date cREATE_DATE) {
		CREATE_DATE = cREATE_DATE;
	}
	public int getSOFT_DELETE() {
		return SOFT_DELETE;
	}
	public void setSOFT_DELETE(int sOFT_DELETE) {
		SOFT_DELETE = sOFT_DELETE;
	}
	public String getID_ANGGOTA_ROMBEL() {
		return ID_ANGGOTA_ROMBEL;
	}
	public void setID_ANGGOTA_ROMBEL(String iD_ANGGOTA_ROMBEL) {
		ID_ANGGOTA_ROMBEL = iD_ANGGOTA_ROMBEL;
	}
	
	

}
