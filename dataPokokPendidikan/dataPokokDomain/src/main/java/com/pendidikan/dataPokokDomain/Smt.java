package com.pendidikan.dataPokokDomain;
import java.io.Serializable;
import java.util.Date;

import org.joda.time.DateTime;
import org.joda.time.LocalDateTime;

public class Smt implements Serializable {
	private String ID;
	private String THN;
	private String NM;
	private Date CREATE_DATE;
	private int SOFT_DELETE;
	
	public String getID() {
		return ID;
	}
	public void setID(String iD) {
		ID = iD;
	}
	public String getTHN() {
		return THN;
	}
	public void setTHN(String tHN) {
		THN = tHN;
	}
	public String getNM() {
		return NM;
	}
	public void setNM(String nM) {
		NM = nM;
	}
	
	public int getSOFT_DELETE() {
		return SOFT_DELETE;
	}
	public void setSOFT_DELETE(int sOFT_DELETE) {
		SOFT_DELETE = sOFT_DELETE;
	}
	public Date getCREATE_DATE() {
		return CREATE_DATE;
	}
	public void setCREATE_DATE(Date cREATE_DATE) {
		CREATE_DATE = cREATE_DATE;
	}
	

}

