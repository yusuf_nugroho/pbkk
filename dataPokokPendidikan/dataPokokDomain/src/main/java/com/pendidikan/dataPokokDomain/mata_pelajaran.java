package com.pendidikan.dataPokokDomain;
import java.io.Serializable;
import java.util.Date;

import org.joda.time.DateTime;

public class mata_pelajaran implements Serializable {
	private String ID_MATPEL;
	private String ID_SAT_MAN;
	private String NAMA_MATPEL;
	private Date CREATE_DATE;
	private int SOFT_DELETE;
	
	public String getID_MATPEL() {
		return ID_MATPEL;
	}
	public void setID_MATPEL(String iD_MATPEL) {
		ID_MATPEL = iD_MATPEL;
	}
	public String getID_SAT_MAN() {
		return ID_SAT_MAN;
	}
	public void setID_SAT_MAN(String iD_SAT_MAN) {
		ID_SAT_MAN = iD_SAT_MAN;
	}
	public String getNAMA_MATPEL() {
		return NAMA_MATPEL;
	}
	public void setNAMA_MATPEL(String nAMA_MATPEL) {
		NAMA_MATPEL = nAMA_MATPEL;
	}
	public Date getCREATE_DATE() {
		return CREATE_DATE;
	}
	public void setCREATE_DATE(Date cREATE_DATE) {
		CREATE_DATE = cREATE_DATE;
	}
	public int getSOFT_DELETE() {
		return SOFT_DELETE;
	}
	public void setSOFT_DELETE(int sOFT_DELETE) {
		SOFT_DELETE = sOFT_DELETE;
	}
}
