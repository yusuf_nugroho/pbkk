package com.pendidikan.dataPokokDomain;

import java.io.Serializable;

import org.joda.time.DateTime;


public class Nilai_semester implements Serializable{

	private String ID_NILAI_SEMESTER;
	private String ID_MATPEL;
	private String ID_NILAI_AKHIR;
	private int NILAI_MATA_PELAJARAN_PENGE;
	private int NILAI_MATA_PELAJARAN_KETERAMPILAN;
	private int NILAI_MATA_PELAJARAN_SIKAP;
	private DateTime CREATE_DATE;
	private Boolean SOFT_DELETE;
	
	public String getID_NILAI_SEMESTER() {
		return ID_NILAI_SEMESTER;
	}
	public void setID_NILAI_SEMESTER(String iD_NILAI_SEMESTER) {
		ID_NILAI_SEMESTER = iD_NILAI_SEMESTER;
	}
	public String getID_MATPEL() {
		return ID_MATPEL;
	}
	public void setID_MATPEL(String iD_MATPEL) {
		ID_MATPEL = iD_MATPEL;
	}
	public String getID_NILAI_AKHIR() {
		return ID_NILAI_AKHIR;
	}
	public void setID_NILAI_AKHIR(String iD_NILAI_AKHIR) {
		ID_NILAI_AKHIR = iD_NILAI_AKHIR;
	}
	public int getNILAI_MATA_PELAJARAN_PENGE() {
		return NILAI_MATA_PELAJARAN_PENGE;
	}
	public void setNILAI_MATA_PELAJARAN_PENGE(int nILAI_MATA_PELAJARAN_PENGE) {
		NILAI_MATA_PELAJARAN_PENGE = nILAI_MATA_PELAJARAN_PENGE;
	}
	public int getNILAI_MATA_PELAJARAN_KETERAMPILAN() {
		return NILAI_MATA_PELAJARAN_KETERAMPILAN;
	}
	public void setNILAI_MATA_PELAJARAN_KETERAMPILAN(
			int nILAI_MATA_PELAJARAN_KETERAMPILAN) {
		NILAI_MATA_PELAJARAN_KETERAMPILAN = nILAI_MATA_PELAJARAN_KETERAMPILAN;
	}
	public int getNILAI_MATA_PELAJARAN_SIKAP() {
		return NILAI_MATA_PELAJARAN_SIKAP;
	}
	public void setNILAI_MATA_PELAJARAN_SIKAP(int nILAI_MATA_PELAJARAN_SIKAP) {
		NILAI_MATA_PELAJARAN_SIKAP = nILAI_MATA_PELAJARAN_SIKAP;
	}
	public DateTime getCREATE_DATE() {
		return CREATE_DATE;
	}
	public void setCREATE_DATE(DateTime cREATE_DATE) {
		CREATE_DATE = cREATE_DATE;
	}
	public Boolean getSOFT_DELETE() {
		return SOFT_DELETE;
	}
	public void setSOFT_DELETE(Boolean sOFT_DELETE) {
		SOFT_DELETE = sOFT_DELETE;
	}

}
