package com.pendidikan.dataPokokDomain;

import java.io.Serializable;
import java.io.Serializable;
import java.util.Date;
import java.util.UUID;


import org.joda.time.DateTime;

public class Satpen implements Serializable{
	private String ID_SAT_MAN;
	private String NM_SAT_MAN;
	private int A_SAT_MAN_AKTIF;
	private String ALAMAT_SATUAN_PENDIDIKAN;
	private String TELP_SATUAN_PENDIDIKAN;
	private String JENJANG_PENDIDIKAN;
	private String BENTUK_PENDIDIKAN;
	private String RAYON;
	private Date CREATE_DATE;
	private int SOFT_DELETE;
	
	public String getID_SAT_MAN() {
		return ID_SAT_MAN;
	}
	public void setID_SAT_MAN(String iD_SAT_MAN) {
		ID_SAT_MAN = iD_SAT_MAN;
	}
	
	public String getALAMAT_SATUAN_PENDIDIKAN() {
		return ALAMAT_SATUAN_PENDIDIKAN;
	}
	public void setALAMAT_SATUAN_PENDIDIKAN(String aLAMAT_SATAUAN_PENDIDIKAN) {
		ALAMAT_SATUAN_PENDIDIKAN = aLAMAT_SATAUAN_PENDIDIKAN;
	}
	public String getTELP_SATUAN_PENDIDIKAN() {
		return TELP_SATUAN_PENDIDIKAN;
	}
	public void setTELP_SATUAN_PENDIDIKAN(String tELP_SATUAN_PENDIDIKAN) {
		TELP_SATUAN_PENDIDIKAN = tELP_SATUAN_PENDIDIKAN;
	}
	public String getJENJANG_PENDIDIKAN() {
		return JENJANG_PENDIDIKAN;
	}
	public void setJENJANG_PENDIDIKAN(String jENJANG_PENDIDIKAN) {
		JENJANG_PENDIDIKAN = jENJANG_PENDIDIKAN;
	}
	public String getBENTUK_PENDIDIKAN() {
		return BENTUK_PENDIDIKAN;
	}
	public void setBENTUK_PENDIDIKAN(String bENTUK_PENDIDIKAN) {
		BENTUK_PENDIDIKAN = bENTUK_PENDIDIKAN;
	}
	public String getRAYON() {
		return RAYON;
	}
	public void setRAYON(String rAYON) {
		RAYON = rAYON;
	}
	public String getNM_SAT_MAN() {
		return NM_SAT_MAN;
	}
	public void setNM_SAT_MAN(String nM_SAT_MAN) {
		NM_SAT_MAN = nM_SAT_MAN;
	}
	public int getA_SAT_MAN_AKTIF() {
		return A_SAT_MAN_AKTIF;
	}
	public void setA_SAT_MAN_AKTIF(int a_SAT_MAN_AKTIF) {
		A_SAT_MAN_AKTIF = a_SAT_MAN_AKTIF;
	}
	public Date getCREATE_DATE() {
		return CREATE_DATE;
	}
	public void setCREATE_DATE(Date cREATE_DATE) {
		CREATE_DATE = cREATE_DATE;
	}
	public int getSOFT_DELETE() {
		return SOFT_DELETE;
	}
	public void setSOFT_DELETE(int sOFT_DELETE) {
		SOFT_DELETE = sOFT_DELETE;
	}

}
