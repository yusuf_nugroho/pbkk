package com.pendidikan.dataPokokDomain;

import java.io.Serializable;
import java.util.Date;

import org.joda.time.DateTime;

public class Mapel_semester implements Serializable {
	private String ID_MAPEL_SEMESTER;
	private String ID_MATPEL;
	private String ID_SMT;
	private Date CREATE_DATE;
	private int SOFT_DELETE;
	
	public String getID_MAPEL_SEMESTER() {
		return ID_MAPEL_SEMESTER;
	}
	public void setID_MAPEL_SEMESTER(String iD_MAPEL_SEMESTER) {
		ID_MAPEL_SEMESTER = iD_MAPEL_SEMESTER;
	}
	public String getID_MATPEL() {
		return ID_MATPEL;
	}
	public void setID_MATPEL(String iD_MATPEL) {
		ID_MATPEL = iD_MATPEL;
	}
	public String getID_SMT() {
		return ID_SMT;
	}
	public void setID_SMT(String iD_SMT) {
		ID_SMT = iD_SMT;
	}
	public Date getCREATE_DATE() {
		return CREATE_DATE;
	}
	public void setCREATE_DATE(Date cREATE_DATE) {
		CREATE_DATE = cREATE_DATE;
	}
	public int getSOFT_DELETE() {
		return SOFT_DELETE;
	}
	public void setSOFT_DELETE(int sOFT_DELETE) {
		SOFT_DELETE = sOFT_DELETE;
	}

}
