package com.pendidikan.dataPokokData.runtest;

import com.pendidikan.dataPokokData.hibernate.*;
import com.pendidikan.dataPokokDomain.*;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import org.hibernate.Session;

import java.util.UUID;

import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.joda.time.DateTime;
import org.joda.time.LocalDateTime;

import com.pendidikan.dataPokokDomain.nilai_akhir;

public class run_nilai_smt {
	public static void main(String[] args) 
    {

		addSmt("25","123","GANJIL",new Date(),1);
		printSmt();
           
}

public static void printSmt() {
	List < Smt > smts = listSmt();
	for (int i = 0; i < smts.size(); i++) {
		Smt smt = smts.get(i);
		System.out.println("##########");
		System.out.println("ID : " + smt.getID());
		System.out.println("Tahun : " + smt.getTHN());
		System.out.print("Nama : " + smt.getNM());
		System.out.println("\n");
	}
}

public static void addSmt(String ID, String THN, String NM, Date date, int delete) {
	Session session = null;
	Transaction txn = null;
	try {
		SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
		session = sessionFactory.openSession();
		txn = session.beginTransaction();
		Smt smt = new Smt();

		//agama.setID_AGAMA(4);
		smt.setID(ID);
		smt.setTHN(THN);
		smt.setNM(NM);
		smt.setCREATE_DATE(date);
		smt.setSOFT_DELETE(delete);
		session.save(smt);
		txn.commit();


	} catch (Exception e) {
		System.out.println(e.getMessage());
	} finally {
		if (!txn.wasCommitted()) {
			txn.rollback();
		}

		session.flush();
		session.close();
	}
}

public static List < Smt > listSmt() {
	Session session = null;
	Transaction txn = null;
	try {
		SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
		session = sessionFactory.openSession();
		txn = session.beginTransaction();
		List < Smt > smts = session.createQuery("FROM Smt").list();
		txn.commit();
		return smts;
	} catch (Exception e) {
		System.out.println(e.getMessage());
	} finally {
		if (!txn.wasCommitted()) {
			txn.rollback();
		}

		session.flush();
		session.close();

	}
	return null;
}

}
