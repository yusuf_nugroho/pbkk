package com.pendidikan.dataPokokData.runtest;

import com.pendidikan.dataPokokData.hibernate.*;
import com.pendidikan.dataPokokDomain.*;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import org.hibernate.Session;

import java.util.UUID;

import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.joda.time.DateTime;
import org.joda.time.LocalDateTime;

import com.pendidikan.dataPokokDomain.Registrasi_pd;

public class run_rombel {
	public static void main(String[] args) 
    {

            String suuid = UUID.randomUUID().toString();
            addrombel(suuid, 1234578, "MTK12","24","IPA2",30, 2,new Date(), 0);
            printRombel();
}

public static void printRombel() {
	List < Rombel> rombel = listRombel();
	for (int i = 0; i < rombel.size(); i++) {
		Rombel rombell = rombel.get(i);
		System.out.println("##########");
		System.out.println("ID_ROMBEL : " + rombell.getID_ROMBEL());
		System.out.println("ID_PTK : " + rombell.getID_PTK());
		System.out.print("ID_MATPEL : " + rombell.getID_MATPEL());
		System.out.print("ID_SMT : " + rombell.getID_SMT());
		System.out.print("NAMA ROMBEL : " + rombell.getNM_ROMBBEL());
		System.out.print("JUMLAH ROMBEL : " + rombell.getJML_ROMBBEL());
		System.out.print("PERTEMUAN SEMINGGU : " + rombell.getPERTEMUAN_DALAM_SEMINGGU());
		System.out.println("\n");
	}
}

public static void addrombel(String ID_ROMBEL, int ID_PTK, String ID_MATPEL, String ID_SMT,String nama,int jumlah, int pertemuan,Date date, int delete) {
	Session session = null;
	Transaction txn = null;
	try {
		SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
		session = sessionFactory.openSession();
		txn = session.beginTransaction();
		Rombel romb = new Rombel();

		romb.setID_ROMBEL(ID_ROMBEL);
		romb.setID_PTK(ID_PTK);
		romb.setID_MATPEL(ID_MATPEL);
		romb.setID_SMT(ID_SMT);
		romb.setNM_ROMBBEL(nama);
		romb.setJML_ROMBBEL(jumlah);
		romb.setPERTEMUAN_DALAM_SEMINGGU(pertemuan);
		romb.setCREATE_DATE(date);
		romb.setSOFT_DELETE(delete);
		session.save(romb);
		txn.commit();


	} catch (Exception e) {
		System.out.println(e.getMessage());
	} finally {
		if (!txn.wasCommitted()) {
			txn.rollback();
		}

		session.flush();
		session.close();
	}
}

public static List < Rombel > listRombel() 
    {
	Session session = null;
	Transaction txn = null;
	try {
		SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
		session = sessionFactory.openSession();
		txn = session.beginTransaction();
		List < Rombel > rombel = session.createQuery("FROM Rombel").list();
		txn.commit();
		return rombel;
	} catch (Exception e) {
		System.out.println(e.getMessage());
	} finally {
		if (!txn.wasCommitted()) {
			txn.rollback();
		}

		session.flush();
		session.close();

	}
	return null;
}

}
