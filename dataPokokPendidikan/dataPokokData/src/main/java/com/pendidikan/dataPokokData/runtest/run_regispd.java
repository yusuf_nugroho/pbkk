package com.pendidikan.dataPokokData.runtest;

import com.pendidikan.dataPokokData.hibernate.*;
import com.pendidikan.dataPokokDomain.*;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import org.hibernate.Session;

import java.util.UUID;

import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.joda.time.DateTime;
import org.joda.time.LocalDateTime;


public class run_regispd {
	public static void main(String[] args) 
    {

            String suuid = UUID.randomUUID().toString();
            addregistrasi(suuid, "0e19224f-f9d7-4b02-b9f0-c35c0a528f90", "ac905cba-d052-4b28-83c7-8809a849a685", new Date(), new Date(), new Date(), 0);
            printRegisPD();
}

public static void printRegisPD() {
	List < Registrasi_pd> registrasi = listRegistrasi_pd();
	for (int i = 0; i < registrasi.size(); i++) {
		Registrasi_pd reg = registrasi.get(i);
		System.out.println("##########");
		System.out.println("ID_REGISTRASI : " + reg.getID_REGISTRASI());
		System.out.println("ID_SATPEN : " + reg.getID_SAT_MAN());
		System.out.print("ID_PD : " + reg.getID_PD());
		System.out.print("TANGGAL_MASUK : " + reg.getTANGGAL_MASUK());
		System.out.print("TANGGAL_KELUAR : " + reg.getTANGGAL_KELUAR());
		System.out.println("\n");
	}
}

public static void addregistrasi(String ID_REG, String ID_SATMAN, String ID_PD, Date tglmasuk, Date tglkeluar, Date date, int delete) {
	Session session = null;
	Transaction txn = null;
	try {
		SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
		session = sessionFactory.openSession();
		txn = session.beginTransaction();
		Registrasi_pd regis = new Registrasi_pd();

		regis.setID_REGISTRASI(ID_REG);
		regis.setID_SAT_MAN(ID_SATMAN);
		regis.setID_PD(ID_PD);
		regis.setTANGGAL_MASUK(tglmasuk);
		regis.setTANGGAL_KELUAR(tglkeluar);
		regis.setCREATE_DATE(date);
		regis.setSOFT_DELETE(delete);
		session.save(regis);
		txn.commit();


	} catch (Exception e) {
		System.out.println(e.getMessage());
	} finally {
		if (!txn.wasCommitted()) {
			txn.rollback();
		}

		session.flush();
		session.close();
	}
}

public static List < Registrasi_pd > listRegistrasi_pd() 
    {
	Session session = null;
	Transaction txn = null;
	try {
		SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
		session = sessionFactory.openSession();
		txn = session.beginTransaction();
		List < Registrasi_pd > registrasi = session.createQuery("FROM Registrasi_pd").list();
		txn.commit();
		return registrasi;
	} catch (Exception e) {
		System.out.println(e.getMessage());
	} finally {
		if (!txn.wasCommitted()) {
			txn.rollback();
		}

		session.flush();
		session.close();

	}
	return null;
}

}
