package com.pendidikan.dataPokokData.runtest;

import com.pendidikan.dataPokokData.hibernate.*;
import com.pendidikan.dataPokokDomain.*;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import org.hibernate.Session;
import java.util.UUID;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.joda.time.DateTime;
import org.joda.time.LocalDateTime;

//Unique ID
import java.util.UUID;

public class run_pd {
	public static void main(String[] args) 
    {
		String id=UUID.randomUUID().toString();
		addpd(id,"8a3fe4ab-f79a-11e4-b777-2cd05a4a81f6","1045","RUCHI","surabaya barat","surabaya",new Date(),"L","AB" ,"islam",new Date(), 1);
		printPd();
		//System.out.println(suuid);
}

public static void printPd() {
	List < Pd > pdd = listPd();
	for (int i = 0; i < pdd.size(); i++) {
		Pd pd = pdd.get(i);
		System.out.println("##########");
		System.out.println("ID PD: " + pd.getID_PD());
		System.out.println("ID WALI : " + pd.getID_WALI());
		System.out.print("NIM PD : " + pd.getNIM_PD());
		System.out.println("nama PD: " + pd.getNM_PD());
		System.out.println("alamat : " + pd.getAL_PD());
		System.out.print("tempat lahir : " + pd.getTMPT_PD());
		System.out.print("tanggal lahir : " + pd.getTGLLHR_PD());
		System.out.print("jenis kelamin : " + pd.getJK_PD());
		System.out.print("golongan darah : " + pd.getGOLONGAN_DARAH_PESERTA_DIDIK());
		System.out.print("Agama PD : " + pd.getAGAMA_PD());
		System.out.println("\n");
		
	}
}

public static void addpd(String ID, String wali, String NIM, String nama, String alamat, String tempat, Date tgl_lahir,String JK, String goldar, String agama, Date date, int delete) {
	Session session = null;
	Transaction txn = null;
	try {
		SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
		session = sessionFactory.openSession();
		txn = session.beginTransaction();
		Pd pd = new Pd();

		pd.setID_PD(ID);
		pd.setID_WALI(wali);
		pd.setNIM_PD(NIM);
		pd.setNM_PD(nama);
		pd.setAL_PD(alamat);
		pd.setTMPT_PD(tempat);
		pd.setTGLLHR_PD(tgl_lahir);
		pd.setJK_PD(JK);
		pd.setGOLONGAN_DARAH_PESERTA_DIDIK(goldar);
		pd.setAGAMA_PD(agama);
		pd.setCREATE_DATE(date);
		pd.setSOFT_DELETE(delete);
		session.save(pd);
		txn.commit();


	} catch (Exception e) {
		System.out.println(e.getMessage());
	} finally {
		if (!txn.wasCommitted()) {
			txn.rollback();
		}

		session.flush();
		session.close();
	}
}

public static List < Pd > listPd() {
	Session session = null;
	Transaction txn = null;
	try {
		SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
		session = sessionFactory.openSession();
		txn = session.beginTransaction();
		List < Pd > pdd = session.createQuery("FROM Pd").list();
		txn.commit();
		return pdd;
	} catch (Exception e) {
		System.out.println(e.getMessage());
	} finally {
		if (!txn.wasCommitted()) {
			txn.rollback();
		}

		session.flush();
		session.close();

	}
	return null;
}

}
