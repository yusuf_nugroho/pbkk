/*
 *
 *@Ready
 */
package com.pendidikan.dataPokokData.runtest;


import com.pendidikan.dataPokokData.hibernate.*;
import com.pendidikan.dataPokokDomain.*;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import org.hibernate.Session;

import java.util.UUID;

import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.joda.time.DateTime;
import org.joda.time.LocalDateTime;

//Unique ID
import java.util.UUID;
public class run_agtrombel {

	/**
	 * @param args
	 */
	public static void main(String[] args) 
        {

		addAgtRombel("123436","ac905cba-d052-4b28-83c7-8809a849a685","dece6275-3075-4913-a7fa-55100ec4f4b3",new Date(), 0);
		printAgtRombel();
		       
	}

	public static void printAgtRombel() {
		List < anggota_rombel > anggota = listAgt_rombel();
		for (int i = 0; i < anggota.size(); i++) {
			anggota_rombel agt = anggota.get(i);
			System.out.println("##########");
			System.out.println("ID : " + agt.getID_ANGGOTA_ROMBEL());
			System.out.println("ID PD : " + agt.getID_PD());
			System.out.println("ID ROMBEL : " + agt.getID_ROMBEL());
			System.out.println("\n");

		}
	}

	public static void addAgtRombel(String ID,String idpd, String id_rombel, Date date, int delete) {
		Session session = null;
		Transaction txn = null;
		try {
			SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
			session = sessionFactory.openSession();
			txn = session.beginTransaction();
			anggota_rombel agtrombel = new anggota_rombel();

			agtrombel.setID_ANGGOTA_ROMBEL(ID);
			agtrombel.setID_PD(idpd);
			agtrombel.setID_ROMBEL(id_rombel);
			agtrombel.setCREATE_DATE(date);
			agtrombel.setSOFT_DELETE(delete);
			session.save(agtrombel);
			txn.commit();


		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			if (!txn.wasCommitted()) {
				txn.rollback();
			}

			session.flush();
			session.close();
		}
	}

	public static List < anggota_rombel > listAgt_rombel() {
		Session session = null;
		Transaction txn = null;
		try {
			SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
			session = sessionFactory.openSession();
			txn = session.beginTransaction();
			List < anggota_rombel > anggota = session.createQuery("FROM anggota_rombel").list();
			txn.commit();
			return anggota;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			if (!txn.wasCommitted()) {
				txn.rollback();
			}

			session.flush();
			session.close();

		}
		return null;
	}
}