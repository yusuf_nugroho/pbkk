/*
 *
 *@Ready
 */
package com.pendidikan.dataPokokData.runtest;


import com.pendidikan.dataPokokData.hibernate.*;
import com.pendidikan.dataPokokDomain.*;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import org.hibernate.Session;

import java.util.UUID;

import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.joda.time.DateTime;
import org.joda.time.LocalDateTime;




//Unique ID
import java.util.UUID;
public class run_thnajar {

	/**
	 * @param args
	 */
	public static void main(String[] args) 
        {

		addThn_ajar("2011", 2012, new Date(), 0);
		printThn();
		       
	}

	public static void printThn() {
		List < Thn_ajaran > tahun_ajaran = listThn_ajaran();
		for (int i = 0; i < tahun_ajaran.size(); i++) {
			Thn_ajaran thn_ajar = tahun_ajaran.get(i);
			System.out.println("##########");
			System.out.println("ID : " + thn_ajar.getID_THN_AJARAN());
			System.out.println("Tahun Ajaran : " + thn_ajar.getTHN_THN_AJARAN());
			System.out.println("\n");

		}
	}

	public static void addThn_ajar(String ID, int THN, Date date, int delete) {
		Session session = null;
		Transaction txn = null;
		try {
			SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
			session = sessionFactory.openSession();
			txn = session.beginTransaction();
			Thn_ajaran tahun = new Thn_ajaran();

			tahun.setID_THN_AJARAN(ID);
			tahun.setTHN_THN_AJARAN(THN);
			tahun.setCREATE_DATE(date);
			tahun.setSOFT_DELETE(delete);
			session.save(tahun);
			txn.commit();


		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			if (!txn.wasCommitted()) {
				txn.rollback();
			}

			session.flush();
			session.close();
		}
	}

	public static List < Thn_ajaran > listThn_ajaran() {
		Session session = null;
		Transaction txn = null;
		try {
			SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
			session = sessionFactory.openSession();
			txn = session.beginTransaction();
			List < Thn_ajaran > thn_ajaran = session.createQuery("FROM Thn_ajaran").list();
			txn.commit();
			return thn_ajaran;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			if (!txn.wasCommitted()) {
				txn.rollback();
			}

			session.flush();
			session.close();

		}
		return null;
	}
}