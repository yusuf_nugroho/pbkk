/*
 *
 *@Ready
 */
package com.pendidikan.dataPokokData.runtest;


import com.pendidikan.dataPokokData.hibernate.*;
import com.pendidikan.dataPokokDomain.*;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import org.hibernate.Session;

import java.util.UUID;

import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.joda.time.DateTime;
import org.joda.time.LocalDateTime;

//Unique ID
import java.util.UUID;
public class run_mapelsmt {

	/**
	 * @param args
	 */
	public static void main(String[] args) 
        {

		addMapelsmt("1234567890","MTK12","24", new Date(), 0);
		printMapelsmt();
		       
	}

	public static void printMapelsmt() {
		List < Mapel_semester > mapel = listMapel_semester();
		for (int i = 0; i < mapel.size(); i++) {
			Mapel_semester mata_pelajarana = mapel.get(i);
			System.out.println("##########");
			System.out.println("ID : " + mata_pelajarana.getID_MAPEL_SEMESTER());
			System.out.println("ID MAPEL : " + mata_pelajarana.getID_MATPEL());
			System.out.println("ID SEMESTER : " + mata_pelajarana.getID_SMT());
			System.out.println("\n");

		}
	}

	public static void addMapelsmt(String ID,String Mapel, String smt, Date date, int delete) {
		Session session = null;
		Transaction txn = null;
		try {
			SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
			session = sessionFactory.openSession();
			txn = session.beginTransaction();
			Mapel_semester matpel = new Mapel_semester();

			matpel.setID_MAPEL_SEMESTER(ID);
			matpel.setID_MATPEL(Mapel);
			matpel.setID_SMT(smt);
			matpel.setCREATE_DATE(date);
			matpel.setSOFT_DELETE(delete);
			session.save(matpel);
			txn.commit();


		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			if (!txn.wasCommitted()) {
				txn.rollback();
			}

			session.flush();
			session.close();
		}
	}

	public static List < Mapel_semester > listMapel_semester() {
		Session session = null;
		Transaction txn = null;
		try {
			SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
			session = sessionFactory.openSession();
			txn = session.beginTransaction();
			List < Mapel_semester > mapel_smt = session.createQuery("FROM Mapel_semester").list();
			txn.commit();
			return mapel_smt;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			if (!txn.wasCommitted()) {
				txn.rollback();
			}

			session.flush();
			session.close();

		}
		return null;
	}
}