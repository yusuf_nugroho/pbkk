/*
 *
 *@Ready
 */
package com.pendidikan.dataPokokData.runtest;


import com.pendidikan.dataPokokData.hibernate.*;
import com.pendidikan.dataPokokDomain.*;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import org.hibernate.Session;

import java.util.UUID;

import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.joda.time.DateTime;
import org.joda.time.LocalDateTime;

//Unique ID
import java.util.UUID;
public class run_mapel {

	/**
	 * @param args
	 */
	public static void main(String[] args) 
        {

		addMata_pelajaran("MTK12","c9143d62-b1f4-4dda-9b5c-28ded603b1e0", "Matematika Kelas 1", new Date(), 0);
		printMapel();
		       
	}

	public static void printMapel() {
		List < mata_pelajaran > mapel = listMata_pelajaran();
		for (int i = 0; i < mapel.size(); i++) {
			mata_pelajaran mata_pelajarana = mapel.get(i);
			System.out.println("##########");
			System.out.println("ID : " + mata_pelajarana.getID_MATPEL());
			System.out.println("ID SATUAN PENDIDIKAN : " + mata_pelajarana.getID_SAT_MAN());
			System.out.println("NAMA MAPEL : " + mata_pelajarana.getNAMA_MATPEL());
			System.out.println("\n");

		}
	}

	public static void addMata_pelajaran(String ID,String Satpen, String Nama, Date date, int delete) {
		Session session = null;
		Transaction txn = null;
		try {
			SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
			session = sessionFactory.openSession();
			txn = session.beginTransaction();
			mata_pelajaran matpel = new mata_pelajaran();

			matpel.setID_MATPEL(ID);
			matpel.setID_SAT_MAN(Satpen);
			matpel.setNAMA_MATPEL(Nama);
			
			matpel.setCREATE_DATE(date);
			matpel.setSOFT_DELETE(delete);
			session.save(matpel);
			txn.commit();


		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			if (!txn.wasCommitted()) {
				txn.rollback();
			}

			session.flush();
			session.close();
		}
	}

	public static List < mata_pelajaran > listMata_pelajaran() {
		Session session = null;
		Transaction txn = null;
		try {
			SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
			session = sessionFactory.openSession();
			txn = session.beginTransaction();
			List < mata_pelajaran > mata_pelajaran = session.createQuery("FROM mata_pelajaran").list();
			txn.commit();
			return mata_pelajaran;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			if (!txn.wasCommitted()) {
				txn.rollback();
			}

			session.flush();
			session.close();

		}
		return null;
	}
}