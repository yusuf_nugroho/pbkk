package com.pendidikan.dataPokokData.runtest;

import com.pendidikan.dataPokokData.hibernate.*;
import com.pendidikan.dataPokokDomain.*;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import org.hibernate.Session;
import java.util.UUID;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.joda.time.DateTime;
import org.joda.time.LocalDateTime;

//Unique ID
import java.util.UUID;

public class run_waliPD {
	public static void main(String[] args) 
    {
		addwali_pd(UUID.randomUUID().toString(),UUID.randomUUID().toString(),UUID.randomUUID().toString(),"kang ha neul","kim so eun",new Date(), 1);
		printwali_Pd();
		//System.out.println(suuid);
}

public static void printwali_Pd() {
	List < Wali_pd > wali = listWali_pd();
	for (int i = 0; i < wali.size(); i++) {
		Wali_pd walii = wali.get(i);
		System.out.println("##########");
		System.out.println("ID WALI: " + walii.getID_WALI());
		System.out.println("NO_AKTE : " + walii.getNO_AKTE());
		System.out.print("NO_KK : " + walii.getNO_KK());
		System.out.println("nama ayah: " + walii.getNAMA_AYAH());
		System.out.println("nama ibu : " + walii.getNAMA_IBU());
		System.out.println("\n");
		
	}
}

public static void addwali_pd(String ID, String akte, String kk, String nama_ayah, String nama_ibu, Date date, int delete) {
	Session session = null;
	Transaction txn = null;
	try {
		SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
		session = sessionFactory.openSession();
		txn = session.beginTransaction();
		Wali_pd wali = new Wali_pd();

		wali.setID_WALI(ID);
		wali.setNO_AKTE(akte);
		wali.setNO_KK(kk);
		wali.setNAMA_AYAH(nama_ayah);
		wali.setNAMA_IBU(nama_ibu);
		wali.setCREATE_DATE(date);
		wali.setSOFT_DELETE(delete);
		session.save(wali);
		txn.commit();


	} catch (Exception e) {
		System.out.println(e.getMessage());
	} finally {
		if (!txn.wasCommitted()) {
			txn.rollback();
		}

		session.flush();
		session.close();
	}
}

public static List < Wali_pd > listWali_pd() {
	Session session = null;
	Transaction txn = null;
	try {
		SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
		session = sessionFactory.openSession();
		txn = session.beginTransaction();
		List < Wali_pd > wali = session.createQuery("FROM Wali_pd").list();
		txn.commit();
		return wali;
	} catch (Exception e) {
		System.out.println(e.getMessage());
	} finally {
		if (!txn.wasCommitted()) {
			txn.rollback();
		}

		session.flush();
		session.close();

	}
	return null;
}

}
