package com.pendidikan.dataPokokData.runtest;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.pendidikan.dataPokokDomain.Ptk;

public class run_ptk {
	public static void main(String[] args) 
    {

            String suuid = UUID.randomUUID().toString();
            addptk(1234578,suuid,"ruchi intan", new Date(), 0);
            printPtk();
           
}

	public static void printPtk()
	{
		List<Ptk> ptk = listPtk();
		for(int i =0 ;i<ptk.size();i++){
			Ptk peteka = ptk.get(i);
			System.out.println("##########");
			System.out.println("ID_PTK : " + peteka.getID_PTK());
			System.out.println("NIP_PTK : " + peteka.getNIP_PTK());
			System.out.print("NAMA_PTK : "+ peteka.getNM_PTK());
			
			System.out.println("\n");
			/*if(agama.getEXPIRED_DATE() == null){
				System.out.println("null");
			}
			else{
				System.out.println(smt.getNM().toString());
			}*/
			 
		}
	}
	
	public static void addptk(int ID, String nip,String nama, Date date, int delete){
		Session session = null;
		Transaction txn = null;
		try {  
		    SessionFactory sessionFactory = 
		        new Configuration().configure().buildSessionFactory();
		    session = sessionFactory.openSession();  
		    txn = session.beginTransaction();
		    Ptk ptka = new Ptk();

		    ptka.setID_PTK(ID);
		    ptka.setNIP_PTK(nip);
		    ptka.setNM_PTK(nama);
		    ptka.setCREATE_DATE(date);
		    ptka.setSOFT_DELETE(delete);
			session.save(ptka);
		    txn.commit();
		    

		} catch (Exception e) { 
		    System.out.println(e.getMessage());
		} finally {
		    if (!txn.wasCommitted()) {
		        txn.rollback();
		    }

		    session.flush();  
		    session.close();   
		}
	}
	
	public static List<Ptk> listPtk(){
		Session session = null;
		Transaction txn = null;
	      try{
	    	  SessionFactory sessionFactory = 
	  		        new Configuration().configure().buildSessionFactory();
	  		    session = sessionFactory.openSession();  
	  		    txn = session.beginTransaction();
	         List<Ptk> ptkk = session.createQuery("FROM Ptk").list(); 
	         txn.commit();
	         return ptkk;
	      }catch (Exception e) {
	         System.out.println(e.getMessage());
	      }finally {
	    	  if (!txn.wasCommitted()) {
			        txn.rollback();
			    }

			    session.flush();  
			    session.close();  
			    
	      }
	      return null;
	   }
	
}
