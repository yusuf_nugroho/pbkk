package com.pendidikan.dataPokokData.runtest;



import com.pendidikan.dataPokokData.hibernate.*;
import com.pendidikan.dataPokokDomain.*;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import org.hibernate.Session;

import java.util.UUID;

import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.joda.time.DateTime;
import org.joda.time.LocalDateTime;


//Unique ID
import java.util.UUID;
public class run_satpen {
	
		public static void main(String[] args) 
	        {

	                String suuid = UUID.randomUUID().toString();
					addsatpen(suuid, "SMAN 5", 1, "surabaya", "0317573178", "SMA", "SMA", "tidak ada", new Date(), 0);
					printSatpen();
					System.out.println(suuid);
	               
		}

		public static void printSatpen() {
			List < Satpen > satpen = listSatpen();
			for (int i = 0; i < satpen.size(); i++) {
				Satpen satpens = satpen.get(i);
				System.out.println("##########");
				System.out.println("ID_SATPEN : " + satpens.getID_SAT_MAN());
				System.out.println("NAMA_SATPEN : " + satpens.getNM_SAT_MAN());
				System.out.print("ALAMAT_SATPEN : " + satpens.getALAMAT_SATUAN_PENDIDIKAN());
				System.out.print("TELEPON_SATPEN : " + satpens.getTELP_SATUAN_PENDIDIKAN());
				System.out.print("JENJANG_SATPEN : " + satpens.getJENJANG_PENDIDIKAN());
				System.out.print("BENTUK_SATPEN : " + satpens.getBENTUK_PENDIDIKAN());
				System.out.print("RAYON : " + satpens.getRAYON());
				System.out.println("\n");
				/*if(agama.getEXPIRED_DATE() == null){
					System.out.println("null");
				}
				else{
					System.out.println(smt.getNM().toString());
				}*/

			}
		}

		public static void addsatpen(String ID_SATMAN, String nama, int status, String alamat, String telp, String jenjang, String bentuk, String rayon, Date date, int delete) {
			Session session = null;
			Transaction txn = null;
			try {
				SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
				session = sessionFactory.openSession();
				txn = session.beginTransaction();
				Satpen satpens = new Satpen();

				satpens.setID_SAT_MAN(ID_SATMAN);
				satpens.setNM_SAT_MAN(nama);
				satpens.setA_SAT_MAN_AKTIF(status);
				satpens.setALAMAT_SATUAN_PENDIDIKAN(alamat);
				satpens.setTELP_SATUAN_PENDIDIKAN(telp);
				satpens.setJENJANG_PENDIDIKAN(jenjang);
				satpens.setBENTUK_PENDIDIKAN(bentuk);
				satpens.setRAYON(rayon);
				satpens.setCREATE_DATE(date);
				satpens.setSOFT_DELETE(delete);
				session.save(satpens);
				txn.commit();


			} catch (Exception e) {
				System.out.println(e.getMessage());
			} finally {
				if (!txn.wasCommitted()) {
					txn.rollback();
				}

				session.flush();
				session.close();
			}
		}

		public static List < Satpen > listSatpen() 
	        {
			Session session = null;
			Transaction txn = null;
			try {
				SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
				session = sessionFactory.openSession();
				txn = session.beginTransaction();
				List < Satpen > satpenn = session.createQuery("FROM Satpen").list();
				txn.commit();
				return satpenn;
			} catch (Exception e) {
				System.out.println(e.getMessage());
			} finally {
				if (!txn.wasCommitted()) {
					txn.rollback();
				}

				session.flush();
				session.close();

			}
			return null;
		}


	}
