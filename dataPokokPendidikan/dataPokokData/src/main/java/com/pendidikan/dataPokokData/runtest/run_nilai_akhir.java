package com.pendidikan.dataPokokData.runtest;

import com.pendidikan.dataPokokData.hibernate.*;
import com.pendidikan.dataPokokDomain.*;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import org.hibernate.Session;

import java.util.UUID;

import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.joda.time.DateTime;
import org.joda.time.LocalDateTime;

import com.pendidikan.dataPokokDomain.nilai_akhir;

public class run_nilai_akhir {
	
	public static void main(String[] args) 
    {
	addnilai_akhir("12346",80,99,new Date(),1);
	printNilai_akhir();
    }
	
	public static void printNilai_akhir() {
		List < nilai_akhir > nakhir = listnilai_akhir();
		for (int i = 0; i < nakhir.size(); i++) {
			nilai_akhir akhir = nakhir.get(i);
			System.out.println("##########");
			System.out.println("ID_NILAI_AKHIR : " + akhir.getID_NILAIAKHIR());
			System.out.println("NILAI_UJIAN_AKHIR : " + akhir.getNILAI_UJIANAKHIR());
			System.out.print("NILAI_UAS : " + akhir.getNILAI_UAS());
			System.out.println("\n");
			/*if(agama.getEXPIRED_DATE() == null){
				System.out.println("null");
			}
			else{
				System.out.println(smt.getNM().toString());
			}*/

		}
	}

	public static void addnilai_akhir(String ID, int nilai_akhirr, int nilai_uas, Date date, int delete) {
		Session session = null;
		Transaction txn = null;
		try {
			SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
			session = sessionFactory.openSession();
			txn = session.beginTransaction();
			nilai_akhir akhir = new nilai_akhir();

			akhir.setID_NILAIAKHIR(ID);
			akhir.setNILAI_UJIANAKHIR(nilai_akhirr);
			akhir.setNILAI_UAS(nilai_uas);
			akhir.setCREATE_DATE(date);
			akhir.setSOFT_DELETE(delete);
			session.save(akhir);
			txn.commit();


		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			if (!txn.wasCommitted()) {
				txn.rollback();
			}

			session.flush();
			session.close();
		}
	}

	public static List < nilai_akhir > listnilai_akhir() {
		Session session = null;
		Transaction txn = null;
		try {
			SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
			session = sessionFactory.openSession();
			txn = session.beginTransaction();
			List < nilai_akhir > nakhir = session.createQuery("FROM nilai_akhir").list();
			txn.commit();
			return nakhir;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			if (!txn.wasCommitted()) {
				txn.rollback();
			}

			session.flush();
			session.close();

		}
		return null;
	}
}
