-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 09 Mei 2015 pada 18.27
-- Versi Server: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `pbkk2`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `anggota_rombel`
--

CREATE TABLE IF NOT EXISTS `anggota_rombel` (
  `ID_ANGGOTA_ROMBEL` varchar(10) NOT NULL,
  `ID_PD` varchar(36) NOT NULL,
  `ID_ROMBEL` varchar(36) DEFAULT NULL,
  `CREATE_DATE` date NOT NULL,
  `SOFT_DELETE` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `mapel_semester`
--

CREATE TABLE IF NOT EXISTS `mapel_semester` (
  `ID_MAPEL_SEMESTER` varchar(10) NOT NULL,
  `ID_MATPEL` varchar(5) DEFAULT NULL,
  `ID_SMT` varchar(15) DEFAULT NULL,
  `CREATE_DATE` date NOT NULL,
  `SOFT_DELETE` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `mata_pelajaran`
--

CREATE TABLE IF NOT EXISTS `mata_pelajaran` (
  `ID_MATPEL` varchar(5) NOT NULL,
  `ID_SAT_MAN` varchar(36) DEFAULT NULL,
  `NAMA_MATPEL` varchar(50) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `SOFT_DELETE` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `nilai_akhir`
--

CREATE TABLE IF NOT EXISTS `nilai_akhir` (
  `ID_NILAI_AKHIR` varchar(5) NOT NULL,
  `NILAI_UJIAN_AKHIR` int(11) NOT NULL,
  `NILAI_UAS` int(11) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `SOFT_DELETE` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `nilai_akhir`
--

INSERT INTO `nilai_akhir` (`ID_NILAI_AKHIR`, `NILAI_UJIAN_AKHIR`, `NILAI_UAS`, `CREATE_DATE`, `SOFT_DELETE`) VALUES
('12345', 80, 99, '2015-05-09', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `nilai_semester`
--

CREATE TABLE IF NOT EXISTS `nilai_semester` (
  `ID_NILAI_SEMESTER` varchar(5) NOT NULL,
  `ID_MATPEL` varchar(5) DEFAULT NULL,
  `ID_NILAI_AKHIR` varchar(5) DEFAULT NULL,
  `NILAI_MATA_PELAJARAN_PENGE` int(11) DEFAULT NULL,
  `NILAI_MATA_PELAJARAN_KETERAMPILAN` int(11) DEFAULT NULL,
  `NILAI_MATA_PELAJARAN_SIKAP` int(11) DEFAULT NULL,
  `CREATE_DATE` date NOT NULL,
  `SOFT_DELETE` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pd`
--

CREATE TABLE IF NOT EXISTS `pd` (
  `ID_PD` varchar(36) NOT NULL,
  `ID_WALI` varchar(36) DEFAULT NULL,
  `NIM_PD` varchar(36) NOT NULL,
  `NM_PD` varchar(255) NOT NULL,
  `AL_PD` varchar(50) DEFAULT NULL,
  `TMPT_PD` varchar(25) DEFAULT NULL,
  `TGLLHR_PD` date DEFAULT NULL,
  `JK_PD` varchar(2) DEFAULT NULL,
  `GOLONGAN_DARAH_PESERTA_DIDIK` varchar(2) DEFAULT NULL,
  `AGAMA_PD` varchar(15) DEFAULT NULL,
  `CREATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `SOFT_DELETE` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pd`
--

INSERT INTO `pd` (`ID_PD`, `ID_WALI`, `NIM_PD`, `NM_PD`, `AL_PD`, `TMPT_PD`, `TGLLHR_PD`, `JK_PD`, `GOLONGAN_DARAH_PESERTA_DIDIK`, `AGAMA_PD`, `CREATE_DATE`, `SOFT_DELETE`) VALUES
('215C936D-7BD4-4426-7726-233DC1FC9A16', '9DEF5B97-4124-5CAA-70C1-0B16F4D36134', '', 'Yu Ahn', 'Kab. Temanggung', 'Kota Padang Sidempuan', '1994-07-29', 'P', 'A', 'Hindu', '2015-05-09 16:14:00', 0),
('30D6F4CD-24F0-365D-4E03-A64E11E94582', '5754F8E1-61AE-81F4-26E5-B29D42221FE7', '', 'Yi-Shiuan Alsamdan', 'Kota Semarang', 'Kab. Sidoarjo', '1994-01-19', 'L', 'A', 'Khonghucu', '2015-05-09 16:14:01', 0),
('548958FC-791B-23C8-150C-A0D64F7B393B', '40142296-7363-31BF-21B8-3A7B19986207', '', 'Zorina Abreu', 'Kab. Labuhanbatu Utara', 'Kab. Aceh Jaya', '1994-03-13', 'L', 'B', 'Islam', '2015-05-09 16:14:00', 0),
('5A00294C-3C81-28E9-5637-A0495B7D22E8', '6EAA9BAA-966B-0A9D-5E99-423BDD62822F', '', 'Yunzhe Afonso', 'Kab. Temanggung', 'Kab. Tuban', '1994-02-09', 'P', 'AB', 'Hindu', '2015-05-09 16:14:00', 0),
('5AF59142-198A-4C5C-7F65-7DDDECE527FA', '1ED340CD-5F4F-5194-960B-613305E47038', '', 'Yasuhiro Au', 'Kab. Serang', 'Kab. Aceh Barat', '1994-04-04', 'L', 'A', 'Islam', '2015-05-09 16:14:01', 0),
('5D734357-63D9-0144-08D7-1B15826C4878', 'C0B2842C-1B8B-759E-1CE5-809D7FBB37FB', '', 'Zhen Abu-Zahra', 'Kab. Lamongan', 'Kota Serang', '1994-06-28', 'P', 'A', 'Kristen', '2015-05-09 16:14:00', 0),
('5E0D9CB6-6E0F-0CA3-7AFB-9FA4EF233545', '2F42DB6E-07B5-521D-43E9-F4B881E6249E', '', 'Zhanetta Adeyeye', 'Kab. Bekasi', 'Kota Salatiga', '1994-09-07', 'L', 'AB', 'Khonghucu', '2015-05-09 16:14:00', 0),
('90390036-7554-21B9-00E4-C517484977B9', 'E83B0762-222D-5EAB-9CD4-4D78109A7F8F', '', 'Yi Aramendia', 'Kab. Humbang Hasundutan', 'Kab. Pidie', '1994-05-23', 'L', 'O', 'Kristen', '2015-05-09 16:14:01', 0),
('99C5115E-A49F-25AB-0FD9-7ECCB7310C44', '275725C4-8BC1-1B2D-047B-75F822571010', '', 'Yaya Ashkenazi', 'Kab. Lebak', 'Kab. Pekalongan', '1994-10-05', 'P', 'O', 'Katolik', '2015-05-09 16:14:01', 0),
('B224F12C-00B6-7880-A37D-FF2273C6437A', 'FEFF7FFF-74DA-00FD-96E1-514FD8BA450B', '', 'Yingda Alter', 'Kota Yogyakarta', 'Kota Tegal', '1994-05-09', 'P', 'O', 'Hindu', '2015-05-09 16:14:01', 0),
('BCBA61DE-71AA-85D0-79AA-B9E64B994865', '582AE723-67FC-567D-4181-137C20E20657', '', 'Youngjin Ahn', 'Kab. Kudus', 'Kota Magelang', '1994-06-16', 'P', 'AB', 'Buddha', '2015-05-09 16:14:00', 0),
('C501BB69-90C0-827E-6AFC-FFA30DC054D0', 'B42B67F7-9B40-8D91-2DCA-12B67A1415FF', '', 'Yookyung Alexander', 'Kota Mojokerto', 'Kab. Tapanuli Selatan', '1994-03-27', 'L', 'O', 'Islam', '2015-05-09 16:14:01', 0),
('D1A60FF1-5ECC-A35E-4066-26DA5BEC7428', 'AD0D08AD-83EB-12EE-7825-16E755FB6B63', '', 'Yoon Akin-Aderibigbe', 'Kab. Kediri', 'Kab. Gayo Lues', '1994-06-30', 'P', 'A', 'Buddha', '2015-05-09 16:14:00', 0),
('D76CC12B-4AFB-8061-456F-5FD4737600F4', '199922B9-3D6F-1CF8-7D81-51B8D11E01F7', '', 'Yi-Feng Alvarez', 'Kota Tangerang Selatan', 'Kab. Bener Meriah', '1994-08-01', 'P', 'A', 'Hindu', '2015-05-09 16:14:01', 0),
('E0462492-3AC4-9FA2-06E3-13A3D1E5633B', 'EF2DAC41-00B7-90BD-9E1A-B6A0E2130344', '', 'Ying Altmann', 'Kota Blitar', 'Kota Langsa', '1994-08-16', 'L', 'A', 'Katolik', '2015-05-09 16:14:01', 0),
('F84B05EF-8206-664C-167F-258443ED6560', '80F500EF-7884-5A06-55F0-5275C41E783A', '', 'Yat-Lun Atri', 'Kab. Samosir', 'Kab. Probolinggo', '1994-01-08', 'L', 'B', 'Khonghucu', '2015-05-09 16:14:01', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `ptk`
--

CREATE TABLE IF NOT EXISTS `ptk` (
  `ID_PTK` int(11) NOT NULL,
  `NIP_PTK` varchar(50) NOT NULL,
  `NM_PTK` varchar(255) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `SOFT_DELETE` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `registrasi_pd`
--

CREATE TABLE IF NOT EXISTS `registrasi_pd` (
  `ID_REGISTRASI` varchar(36) NOT NULL,
  `ID_SAT_MAN` varchar(36) DEFAULT NULL,
  `ID_PD` varchar(36) DEFAULT NULL,
  `TANGGAL_MASUK` date NOT NULL,
  `TANGGAL_KELUAR` date NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `SOFT_DELETE` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `rombel`
--

CREATE TABLE IF NOT EXISTS `rombel` (
  `ID_ROMBEL` varchar(36) NOT NULL,
  `ID_PTK` int(11) DEFAULT NULL,
  `ID_MATPEL` varchar(5) DEFAULT NULL,
  `ID_SMT` varchar(15) DEFAULT NULL,
  `NM_ROMBBEL` varchar(255) NOT NULL,
  `JML_ROMBBEL` int(11) NOT NULL,
  `PERTEMUAN_DALAM_SEMINGGU` int(11) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `SOFT_DELETE` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `sat_pen`
--

CREATE TABLE IF NOT EXISTS `sat_pen` (
  `ID_SAT_MAN` varchar(36) NOT NULL,
  `NM_SAT_MAN` varchar(255) NOT NULL,
  `A_SAT_MAN_AKTIF` smallint(6) NOT NULL,
  `ALAMAT_SATUAN_PENDIDIKAN` varchar(500) NOT NULL,
  `TELP_SATUAN_PENDIDIKAN` varchar(15) NOT NULL,
  `JENJANG_PENDIDIKAN` varchar(10) NOT NULL,
  `BENTUK_PENDIDIKAN` varchar(10) NOT NULL,
  `RAYON` varchar(20) DEFAULT NULL,
  `CREATE_DATE` date NOT NULL,
  `SOFT_DELETE` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `smt`
--

CREATE TABLE IF NOT EXISTS `smt` (
  `ID_SMT` varchar(15) NOT NULL,
  `ID_THN_AJARAN` varchar(10) DEFAULT NULL,
  `NM_SMT` varchar(50) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `SOFT_DELETE` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `smt`
--

INSERT INTO `smt` (`ID_SMT`, `ID_THN_AJARAN`, `NM_SMT`, `CREATE_DATE`, `SOFT_DELETE`) VALUES
('24', '123', 'GANJIL', '2015-05-09', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `thn_ajaran`
--

CREATE TABLE IF NOT EXISTS `thn_ajaran` (
  `ID_THN_AJARAN` varchar(10) NOT NULL,
  `THN_THN_AJARAN` int(11) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `SOFT_DELETE` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `thn_ajaran`
--

INSERT INTO `thn_ajaran` (`ID_THN_AJARAN`, `THN_THN_AJARAN`, `CREATE_DATE`, `SOFT_DELETE`) VALUES
('123', 2015, '2015-05-05', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `wali_pd`
--

CREATE TABLE IF NOT EXISTS `wali_pd` (
  `ID_WALI` varchar(36) NOT NULL,
  `ID_PD` varchar(36) DEFAULT NULL,
  `NO_AKTE` varchar(36) NOT NULL,
  `NO_KK` varchar(36) DEFAULT NULL,
  `NAMA_AYAH` varchar(50) NOT NULL,
  `NAMA_IBU` varchar(50) NOT NULL,
  `CREATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `SOFT_DELETE` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `wali_pd`
--

INSERT INTO `wali_pd` (`ID_WALI`, `ID_PD`, `NO_AKTE`, `NO_KK`, `NAMA_AYAH`, `NAMA_IBU`, `CREATE_DATE`, `SOFT_DELETE`) VALUES
('199922B9-3D6F-1CF8-7D81-51B8D11E01F7', NULL, 'BEEF1DF2-6B26-2175-5BA0-65B0B0EFA5EC', '2BFA77E0-49A2-99BA-4B5B-410483C25446', 'Vivek Beistegui', 'Tara Cantrock', '2015-05-09 15:58:53', 0),
('1ED340CD-5F4F-5194-960B-613305E47038', NULL, '9FB5DC2B-254C-2FB9-8FF5-05CEACA89B0E', 'A1CE2A6D-26E5-0BE4-5B9E-37D6560372C0', 'Trent Blakely', 'Sung Casasayas', '2015-05-09 15:58:53', 0),
('275725C4-8BC1-1B2D-047B-75F822571010', NULL, '89CA3682-3461-3BA4-0DE9-0C515CB24F84', '84CBB083-6693-26F1-8DF8-2AE905006FEB', 'Valle Berkman', 'Tae Carrillo', '2015-05-09 15:58:53', 0),
('2F42DB6E-07B5-521D-43E9-F4B881E6249E', NULL, 'E734D688-026E-9054-0062-CCE571089270', 'C1B6C8D7-3514-0503-7C5D-C23602DD0514', 'Yael Bala', 'Tiffanie Bosson', '2015-05-09 15:58:53', 0),
('40142296-7363-31BF-21B8-3A7B19986207', NULL, 'F7061DA3-421C-2825-8794-75BEDB0108C0', '54752BCD-5E17-388B-1E43-F2B2C91A89F0', 'Yan Austin', 'Tobias Blattman', '2015-05-09 15:16:17', 0),
('5754F8E1-61AE-81F4-26E5-B29D42221FE7', NULL, '675DF15B-71BE-901B-1A04-52892AB15616', '95EB77CA-68B0-18B9-5E5B-52E9C28096D0', 'William Baxter', 'Thanadtha Brown', '2015-05-09 15:58:53', 0),
('582AE723-67FC-567D-4181-137C20E20657', NULL, 'E79F63F6-0F60-3FF3-00DA-BE9BAEA9667A', 'DC6E8FC9-05CA-5695-7255-ED5C29AC35CD', 'Won Barakat', 'Thomas Braun', '2015-05-09 15:58:53', 0),
('6EAA9BAA-966B-0A9D-5E99-423BDD62822F', NULL, '3DEC1F4A-2C41-A33A-5133-86B829AA9659', '5C6DF6AA-84B8-8E9E-3515-49343C231E04', 'Wushen Banovac', 'Thomas Brandt', '2015-05-09 15:58:53', 0),
('80F500EF-7884-5A06-55F0-5275C41E783A', NULL, '80E1C062-A3D4-6A5D-33BE-30CA8D5E7F6E', '7A20D5A6-A26B-529A-A0B7-8D25507B79C4', 'Uros Birkhofer', 'Tadamitsu Carrow', '2015-05-09 15:58:53', 0),
('9DEF5B97-4124-5CAA-70C1-0B16F4D36134', NULL, 'F766DB62-0938-22CB-8A68-A07C5E5E817F', '8D90B92C-3221-3AE1-92E3-0BCC465056DA', 'Won Barnes', 'Theodore Brodsky', '2015-05-09 15:58:53', 0),
('AD0D08AD-83EB-12EE-7825-16E755FB6B63', NULL, '31BEF09C-2EBC-2924-4527-8EEDB1031871', '1AE9B4F5-9437-8256-A164-ED6A5F898B8B', 'William Barrett', 'Thavin Bromberg', '2015-05-09 15:58:53', 0),
('B42B67F7-9B40-8D91-2DCA-12B67A1415FF', NULL, '7B8356E5-7704-950F-6973-EEF0AD2D37E3', 'A52F5BDC-4B37-5D5C-4C21-5CCF2E552639', 'Willem Baxter', 'Ted Brown', '2015-05-09 15:58:53', 0),
('C0B2842C-1B8B-759E-1CE5-809D7FBB37FB', NULL, '723DE4F8-8963-7B0D-20F7-8ECE1E0A0BB1', '5C32CE74-652C-52F6-0AA6-8EDFB5DD543E', 'Ya-Han Bagdat', 'Timothy Bolton', '2015-05-09 15:58:52', 0),
('E83B0762-222D-5EAB-9CD4-4D78109A7F8F', NULL, 'F9B4254D-15DC-63D3-984D-E23980BE09BB', '9B5747A0-598D-944A-9C84-CFAC663B8372', 'Viacheslav Benchimol', 'Tanzeer Cao', '2015-05-09 15:58:53', 0),
('EF2DAC41-00B7-90BD-9E1A-B6A0E2130344', NULL, '736845CF-5F29-1522-6D89-552B804538BA', '4001FF16-3C37-27B2-7595-81E590B57573', 'Wayne Bechrakis', 'Taryn Canal', '2015-05-09 15:58:53', 0),
('FEFF7FFF-74DA-00FD-96E1-514FD8BA450B', NULL, '1BC636DF-9264-5CF5-011D-CC31BC6F0684', 'CEABF605-824A-09F7-0255-972CBED127FA', 'Wei Beatty', 'Taylor Buckley', '2015-05-09 15:58:53', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `anggota_rombel`
--
ALTER TABLE `anggota_rombel`
 ADD PRIMARY KEY (`ID_ANGGOTA_ROMBEL`), ADD UNIQUE KEY `ANGGOTA_ROMBEL_PK` (`ID_ANGGOTA_ROMBEL`), ADD KEY `MERUPAKAN_FK` (`ID_PD`), ADD KEY `TERDIRI_DARI_FK` (`ID_ROMBEL`);

--
-- Indexes for table `mapel_semester`
--
ALTER TABLE `mapel_semester`
 ADD PRIMARY KEY (`ID_MAPEL_SEMESTER`), ADD UNIQUE KEY `MAPEL_SEMESTER_PK` (`ID_MAPEL_SEMESTER`), ADD KEY `RELATIONSHIP_17_FK` (`ID_MATPEL`), ADD KEY `RELATIONSHIP_18_FK` (`ID_SMT`);

--
-- Indexes for table `mata_pelajaran`
--
ALTER TABLE `mata_pelajaran`
 ADD PRIMARY KEY (`ID_MATPEL`), ADD UNIQUE KEY `MATA_PELAJARAN_PK` (`ID_MATPEL`), ADD KEY `MEMPUNYAI_MAPEL_FK` (`ID_SAT_MAN`);

--
-- Indexes for table `nilai_akhir`
--
ALTER TABLE `nilai_akhir`
 ADD PRIMARY KEY (`ID_NILAI_AKHIR`), ADD UNIQUE KEY `NILAI_AKHIR_PK` (`ID_NILAI_AKHIR`);

--
-- Indexes for table `nilai_semester`
--
ALTER TABLE `nilai_semester`
 ADD PRIMARY KEY (`ID_NILAI_SEMESTER`), ADD UNIQUE KEY `NILAI_SEMESTER_PK` (`ID_NILAI_SEMESTER`), ADD KEY `RELATIONSHIP_22_FK` (`ID_NILAI_AKHIR`), ADD KEY `RELATIONSHIP_23_FK` (`ID_MATPEL`);

--
-- Indexes for table `pd`
--
ALTER TABLE `pd`
 ADD PRIMARY KEY (`ID_PD`), ADD UNIQUE KEY `PD_PK` (`ID_PD`), ADD KEY `RELATIONSHIP_16_FK` (`ID_WALI`);

--
-- Indexes for table `ptk`
--
ALTER TABLE `ptk`
 ADD PRIMARY KEY (`ID_PTK`), ADD UNIQUE KEY `PTK_PK` (`ID_PTK`);

--
-- Indexes for table `registrasi_pd`
--
ALTER TABLE `registrasi_pd`
 ADD PRIMARY KEY (`ID_REGISTRASI`), ADD UNIQUE KEY `REGISTRASI_PD_PK` (`ID_REGISTRASI`), ADD KEY `RELATIONSHIP_12_FK` (`ID_PD`), ADD KEY `RELATIONSHIP_13_FK` (`ID_SAT_MAN`);

--
-- Indexes for table `rombel`
--
ALTER TABLE `rombel`
 ADD PRIMARY KEY (`ID_ROMBEL`), ADD UNIQUE KEY `ROMBEL_PK` (`ID_ROMBEL`), ADD KEY `MENGAJAR_FK` (`ID_PTK`), ADD KEY `MEMILIKI_ROMBONGAN_BELAJAR_FK` (`ID_MATPEL`), ADD KEY `RELATIONSHIP_14_FK` (`ID_SMT`);

--
-- Indexes for table `sat_pen`
--
ALTER TABLE `sat_pen`
 ADD PRIMARY KEY (`ID_SAT_MAN`), ADD UNIQUE KEY `SAT_PEN_PK` (`ID_SAT_MAN`);

--
-- Indexes for table `smt`
--
ALTER TABLE `smt`
 ADD PRIMARY KEY (`ID_SMT`), ADD UNIQUE KEY `SMT_PK` (`ID_SMT`), ADD KEY `PADA_SAAT_FK` (`ID_THN_AJARAN`);

--
-- Indexes for table `thn_ajaran`
--
ALTER TABLE `thn_ajaran`
 ADD PRIMARY KEY (`ID_THN_AJARAN`), ADD UNIQUE KEY `THN_AJARAN_PK` (`ID_THN_AJARAN`);

--
-- Indexes for table `wali_pd`
--
ALTER TABLE `wali_pd`
 ADD PRIMARY KEY (`ID_WALI`), ADD UNIQUE KEY `WALI_PD_PK` (`ID_WALI`), ADD KEY `RELATIONSHIP_15_FK` (`ID_PD`);

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `anggota_rombel`
--
ALTER TABLE `anggota_rombel`
ADD CONSTRAINT `FK_ANGGOTA__MERUPAKAN_PD` FOREIGN KEY (`ID_PD`) REFERENCES `pd` (`ID_PD`),
ADD CONSTRAINT `FK_ANGGOTA__TERDIRI_D_ROMBEL` FOREIGN KEY (`ID_ROMBEL`) REFERENCES `rombel` (`ID_ROMBEL`);

--
-- Ketidakleluasaan untuk tabel `mapel_semester`
--
ALTER TABLE `mapel_semester`
ADD CONSTRAINT `FK_MAPEL_SE_RELATIONS_MATA_PEL` FOREIGN KEY (`ID_MATPEL`) REFERENCES `mata_pelajaran` (`ID_MATPEL`),
ADD CONSTRAINT `FK_MAPEL_SE_RELATIONS_SMT` FOREIGN KEY (`ID_SMT`) REFERENCES `smt` (`ID_SMT`);

--
-- Ketidakleluasaan untuk tabel `mata_pelajaran`
--
ALTER TABLE `mata_pelajaran`
ADD CONSTRAINT `FK_MATA_PEL_MEMPUNYAI_SAT_PEN` FOREIGN KEY (`ID_SAT_MAN`) REFERENCES `sat_pen` (`ID_SAT_MAN`);

--
-- Ketidakleluasaan untuk tabel `nilai_semester`
--
ALTER TABLE `nilai_semester`
ADD CONSTRAINT `FK_NILAI_SE_RELATIONS_MATA_PEL` FOREIGN KEY (`ID_MATPEL`) REFERENCES `mata_pelajaran` (`ID_MATPEL`),
ADD CONSTRAINT `FK_NILAI_SE_RELATIONS_NILAI_AK` FOREIGN KEY (`ID_NILAI_AKHIR`) REFERENCES `nilai_akhir` (`ID_NILAI_AKHIR`);

--
-- Ketidakleluasaan untuk tabel `pd`
--
ALTER TABLE `pd`
ADD CONSTRAINT `FK_PD_RELATIONS_WALI_PD` FOREIGN KEY (`ID_WALI`) REFERENCES `wali_pd` (`ID_WALI`);

--
-- Ketidakleluasaan untuk tabel `registrasi_pd`
--
ALTER TABLE `registrasi_pd`
ADD CONSTRAINT `FK_REGISTRA_RELATIONS_PD` FOREIGN KEY (`ID_PD`) REFERENCES `pd` (`ID_PD`),
ADD CONSTRAINT `FK_REGISTRA_RELATIONS_SAT_PEN` FOREIGN KEY (`ID_SAT_MAN`) REFERENCES `sat_pen` (`ID_SAT_MAN`);

--
-- Ketidakleluasaan untuk tabel `rombel`
--
ALTER TABLE `rombel`
ADD CONSTRAINT `FK_ROMBEL_MEMILIKI__MATA_PEL` FOREIGN KEY (`ID_MATPEL`) REFERENCES `mata_pelajaran` (`ID_MATPEL`),
ADD CONSTRAINT `FK_ROMBEL_MENGAJAR_PTK` FOREIGN KEY (`ID_PTK`) REFERENCES `ptk` (`ID_PTK`),
ADD CONSTRAINT `FK_ROMBEL_RELATIONS_SMT` FOREIGN KEY (`ID_SMT`) REFERENCES `smt` (`ID_SMT`);

--
-- Ketidakleluasaan untuk tabel `smt`
--
ALTER TABLE `smt`
ADD CONSTRAINT `FK_SMT_PADA_SAAT_THN_AJAR` FOREIGN KEY (`ID_THN_AJARAN`) REFERENCES `thn_ajaran` (`ID_THN_AJARAN`);

--
-- Ketidakleluasaan untuk tabel `wali_pd`
--
ALTER TABLE `wali_pd`
ADD CONSTRAINT `FK_WALI_PD_RELATIONS_PD` FOREIGN KEY (`ID_PD`) REFERENCES `pd` (`ID_PD`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
